let buttons = document.querySelectorAll(".btn");
let lastButton = null;
document.addEventListener("keydown", (event) => {
    if (lastButton !== null) {
        lastButton.classList.remove("active");
        lastButton.classList.add("inactive");
    }
    for (const btn of buttons) {
        if (event.key.toUpperCase() === btn.innerText.toUpperCase()) {
            if (btn.classList.contains("inactive")) {
                btn.classList.remove("inactive");
                btn.classList.add("active");
            }
            lastButton = btn;
        }
    }
});
